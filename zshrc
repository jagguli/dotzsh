#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#
# eval "$(rash init)"
# Source Prezto.
#source ~/.ssh/environment
fpath=(~/.zsh/functions.d $fpath)
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...

#source ~/.profile
#source ~/.zsh/zprofile
if [[ -s "${ZDOTDIR:-$HOME}/.aliases" ]]; then
    source "${ZDOTDIR:-$HOME}/.aliases"
fi


setopt HIST_REDUCE_BLANKS
#setopt HIST_IGNORE_SPACE
#setopt SH_WORD_SPLIT
setopt extendedglob
setopt NO_BEEP
#setopt AUTO_CD
setopt CORRECT
setopt PROMPT_SUBST
setopt INC_APPEND_HISTORY_TIME

# this works in some cases
[[ -n "${key[Up]}"      ]] && bindkey  "${key[Up]}"      history-beginning-search-backward
[[ -n "${key[Down]}"    ]] && bindkey  "${key[Down]}"    history-beginning-search-forward
# Search backwards and forwards with a pattern
bindkey -M vicmd '/' history-incremental-pattern-search-backward
bindkey -M vicmd '?' history-incremental-pattern-search-forward

# set up for insert mode too
bindkey -M viins '^[[A' history-beginning-search-backward
bindkey -M viins '^[[B' history-beginning-search-forward



bindkey '^R' history-incremental-search-backward
bindkey  end-of-line
bindkey  kill-line
# Enable auto-execution of functions.
typeset -ga preexec_functions
typeset -ga precmd_functions
typeset -ga chpwd_functions

# Enable Ctrl-x-e to edit command line
autoload -U edit-command-line
# Emacs style
zle -N edit-command-line
bindkey '^xe' edit-command-line
bindkey '^x^e' edit-command-line
# Vi style:
# zle -N edit-command-line
# bindkey -M vicmd v edit-command-line
#
#zstyle :omz:plugins:ssh-agent agent-forwarding on
#compctl -/ cd
# Initialize colors.
#autoload -U colors
#colors
#autoload -U compinit
#compinit
#autoload colors ; colors
#autoload -U promptinit
#promptinit
# Allow for functions in the prompt.
# Autoload zsh functions.
cdpath=( ~/streethawk/ /etc ~/devel/ ~/ ~/share)
#autoload -U ~/.zsh/functions/*(:t)
zstyle ':completion:*:*:vim:*:*files' ignored-patterns '*.pyc'
#zstyle ':completion:*' completer  _oldlist _expand _force_rehash _complete
# tab completion for PID :D
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==34=34}:${(s.:.)LS_COLORS}")';
zstyle ':completion:*' list-colors ''
if [ -d ~/.zsh/functions.d ]; then
    for i in ~/.zsh/functions.d/*.zsh; do
        if [ -r $i ]; then
            . $i
        fi
    done
    unset i
fi
#fortune -s
source ~/.zshrc.local


# Type `Ctrl-x r` to start isearch
# bindkey "^r" rash-zle-isearch

case "$TERM" in
"dumb")
        PS1="> "
        ;;
esac
#setopt -x

#function prompt_cmd(){
#    history -a
#    source ~/.zsh/prompt_jagguli_setup
#}
#
#PROMPT_COMMAND='source ~/.zsh/prompt_jagguli_setup'
#keyboard_config
#VAR=`envoy -l 2>/dev/null|grep .ssh`
#if [ -z "$VAR" ]; then
#    envoy -a 2>/dev/null
#fi
#source <(envoy -p 2>/dev/null)
##stty erase '^H'
#
systemctl --user import-environment PATH &> /dev/null
#
#if [ -z "$DISPLAY" ]; then
#    sysu+ qtile
#fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
