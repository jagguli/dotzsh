function index_project(){
    touch .eproject
    pycscope.py -R . &
    find . -name "*.py"|grep -v '#' | etags -D -I --output TAGS - &
    mkid -m ~/.idutils_map &
    updatedb -n \*.pyc -l 0 -o locatedb -U ./ &
    recollindex -c ~/.recoll &>/dev/null
}
