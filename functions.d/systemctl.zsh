function sysr(){
    ssh $2 'sudo systemctl $1 $3 $@'
}
alias sysr\?='sysr status '
alias sysr\@='sysr restart '
alias sysr+='sysr start '
alias sysr-='sysr stop '


alias sys='sudo systemctl'
alias sysu='systemctl --user'
alias sys\?='sudo systemctl status '
alias sysu\?='systemctl --user status '

alias sys\@='sudo systemctl restart '
alias sysu\@='systemctl --user restart '

alias sys-='sudo systemctl stop '
alias sys--='sudo systemctl disable '
alias sysu-='systemctl --user stop '
alias sysu--='systemctl --user disable '

alias sys+='sudo systemctl start '
alias sys++='sudo systemctl enable '
alias sysu+='systemctl --user start '
alias sysu++='systemctl --user enable '

alias jc='sudo journalctl'
alias jcf='sudo journalctl   -n 300 -f'
alias jcx='sudo journalctl   -n 300 -x'
alias jcfx='sudo journalctl  -n 300 -fx'

alias jcu='journalctl --user-unit'
alias jcfu='journalctl   -n 300 -f --user-unit'
alias jcxu='journalctl   -n 300 -x --user-unit'
alias jcfxu='journalctl  -n 300 -fx --user-unit'

function jcr(){
    ssh $1 'sudo journalctl $@'
}

function jcfx(){
    ssh $1 'sudo journalctl -n 300 -fx $@'
}
