PUSHBULLET_DEVICE_LIST=~/.pushbullet_devices
function pushbullet(){
    command=$1
    shift
    passwd=`pass internet/pushbullet`
    device=`cat $PUSHBULLET_DEVICE_LIST |percol|cut -f1 -d\ `
    pushbullet_cmd.py $passwd $command $device $@
}

function pushbullet_update_devices(){
    passwd=`pass internet/pushbullet`
    pushbullet_cmd.py $passwd getdevices >! $PUSHBULLET_DEVICE_LIST
}
