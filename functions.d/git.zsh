#-*- mode: sh; ... -*-
git_find(){
    git grep $1 $(git rev-list --all)
}

gg_replace() {
  if [[ "$#" == "0" ]]; then
    echo 'Usage:'
    echo '  gg_replace term replacement file_mask'
    echo
    echo 'Example:'
    echo '  gg_replace cappuchino cappuccino *.html'
    echo
  else
    find=$1; shift
    replace=$1; shift

    ORIG_GLOBIGNORE=$GLOBIGNORE
    GLOBIGNORE=*.*
    
    if [[ "$#" = "0" ]]; then
      set -- ' ' $@
    fi

    while [[ "$#" -gt "0" ]]; do
      for file in `git grep -l $find -- $1`; do
        sed -e "s/$find/$replace/g" -i'' $file
      done
      shift
    done

    GLOBIGNORE=$ORIG_GLOBIGNORE
  fi
}
gg_dasherize() {
  gg_replace $1 `echo $1 | sed -e 's/_/-/g'` $2
}
#http://www.davidverhasselt.com/2010/09/14/git-how-to-remove-your-password-from-a-repository/
git_replace_hisory(){
  if [[ "$#" == "0" ]]; then
    echo 'Usage:'
    echo '  git_replace_history term replacement'
    echo
  else
    git filter-branch --tree-filter "find . -type f -exec sed -i -e 's/$1/$2/g' {} \;"
  fi
}

git_grep_history(){
    git filter-branch --tree-filter "grep -r $1 * || true"
}

#https://help.github.com/articles/remove-sensitive-data
git_delete_file_history(){
  if [[ "$#" == "0" ]]; then
    echo 'Usage:'
    echo '  git_delete_file_history file_to_delete'
    echo
  else
    git filter-branch --force --index-filter \
          'git rm --cached --ignore-unmatch \$\1' \
          --prune-empty --tag-name-filter cat -- --all && \
    rm -rf .git/refs/original/ && \
    git reflog expire --expire=now --all &&\
    git gc --prune=now &&\
    git gc --aggressive --prune=now 
  fi
}
alias gitb='git checkout --track origin/master -b'
alias gitp='git stash && git pull -r && git stash pop'
alias gitrf='git review --finish'

evil_git_num_untracked_files() {
    #http://stackoverflow.com/questions/2657935/checking-for-a-dirty-index-or-untracked-files-with-git
    expr `git status --porcelain 2>/dev/null| grep "^??" | wc -l` 
}

function evil_git_dirty {
  [[ $(git diff --shortstat 2> /dev/null | tail -n1) != "" ]] && echo "*"
}

git_reset_pick(){
    PAGER= git lg -$[$1+4]
    if [[ -z $(evil_git_dirty) ]]; then
        git tag old && git reset --hard HEAD~$[$1+1] && git cherry-pick old  && git tag -d old
        PAGER= git lg -$[$1+4]
    else
        echo "modified files"
        git status 
    fi
}

git_review(){
git branch -D  t/devel/review
git co -b t/devel/review origin/master
git cp master
git review
git branch -D  t/devel/review
}
