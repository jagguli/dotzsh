#!/bin/sh -x
BASE_DIR=/usr/local/etc/zsh
if [ ! -d $BASE_DIR ]; then
    BASE_DIR=$HOME/.zsh
fi
if [ -z "$NO_GIT_UPDATE" ]; then
git submodule update  --init --recursive
fi
ln -sf $BASE_DIR/zprofile    $HOME/.zprofile  
ln -sf $BASE_DIR/zshenv      $HOME/.zshenv    
ln -sf $BASE_DIR/zshrc       $HOME/.zshrc     
ln -sf $BASE_DIR/aliases     $HOME/.aliases
ln -sf $BASE_DIR/zshrc.arch  $HOME/.zshrc.arch
ln -sf $BASE_DIR/zprezto     $HOME/.zprezto
ln -sf $BASE_DIR/zpreztorc   $HOME/.zpreztorc
touch $HOME/.zshrc.local
