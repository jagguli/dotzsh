#
# Executes commands at login pre-zshrc.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

export PATH=~/.local/bin:~/.bin:~/.zsh/bin/:~/.gem/ruby/1.9.1/bin:/usr/sbin/:/sbin/:~/.npm/bin/:~/.gem/ruby/2.2.0/bin:$PATH
export SUDO_EDITOR="/usr/bin/vim -p -X"
export HISTFILE=~/.zhistfile
export HISTSIZE=10000
export SAVEHIST=10000
export EDITOR=vim
export AUTOSSH_POLL=5


export PYTHONSTARTUP=~/.local/lib/python2.7/pythonstartup.py

#export XDG_MENU_PREFIX=""
#export XDG_CONFIG_HOME="${HOME}/.config"
#export XDG_DATA_HOME="${HOME}/.local/share"
#
#export XDG_CACHE_HOME="${HOME}/.cache"
#if [ ! -z $XDG_DATA_DIRS ]; then
#  export XDG_DATA_DIRS=$XDG_DATA_DIRS:/usr/share:/usr/local/share
#else
#  export XDG_DATA_DIRS="/usr/share:/usr/local/share"
#fi
#if [ ! -z $XDG_CONFIG_DIRS ]; then
#  export XDG_CONFIG_DIRS=$XDG_CONFIG_DIRS:/etc/xdg
#else
#  export XDG_CONFIG_DIRS="/etc/xdg"
#fi
#
#export XDG_RUNTIME_DIR=/run/user/1000

export KDE_SESSION_ID=1
export QT_PLUGIN_PATH=$HOME/.kde4/lib/kde4/plugins/:/usr/lib/kde4/plugins/

export CPM_USE_NODE=1
export PACKAGES_PATH=~/www/var/lib/

export TERM=xterm-256color
export TZ=Australia/Sydney
export SSH_AUTH_SOCK=/run/user/1000/gnupg/S.gpg-agent.ssh; export SSH_AUTH_SOCK;